let index = process.argv.indexOf("--mode") + 1;
let mode = process.argv[index];

if(mode !== 'prod'){
  require('dotenv').config();
}

module.exports = {
  MODE: mode || 'dev',
  JWT_SECRET: '(abCDefGHijKLmnOPqrSTuvWXyz)',
  MONGO_DATABASE: process.env.MONGO_DATABASE,
  MONGO_HOST: process.env.MONGO_HOST
}
