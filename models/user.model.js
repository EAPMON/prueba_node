const mongoose = require('mongoose');
const { Schema } = mongoose;
const { compareSync, genSaltSync, hashSync} = require('bcryptjs');

const UserSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true,
    unique: true
  },
  password: {
    type: String,
    required: true,
  },
  isAdmin: Boolean
})

UserSchema.pre('save', async function(next){
  const user = this;
  if(!user.isModified('password')){
    return next();
  }

  const salt = genSaltSync(10);
  const hashPassword = hashSync(user.password, salt);
  user.password = hashPassword;

  return next();
});

UserSchema.methods.comparePassword = function(password){  
  return compareSync(password, this.password)
}

module.exports = mongoose.model('user', UserSchema);