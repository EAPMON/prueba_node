const mongoose = require('mongoose');
const { Schema } = mongoose;

const UserSchema = new Schema({
  word: {
    type: String,
    required: true
  },
  author: {
    type: Schema.Types.ObjectId,
    ref: "user",
    required: true,
  },
  isPalindrome: Boolean

})

module.exports = mongoose.model('word', UserSchema);