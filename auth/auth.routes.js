const authServer = require('./index');
const { login, logout } = require('./auth.controller');

authServer.routing('/login', login);
authServer.routing('/logout', logout);