const { MODE } = require('../config/index');

const envs = {
  dev: {
    host: '127.0.0.1',
    port: 8015
  },
  prod: {
    host: '0.0.0.0',
    port: 8015
  }
}

const env = envs[MODE];

module.exports = {
  ENV: env
}
