const {sendError} = require('../helpers/error.helper');
const User = require('../models/user.model');
const { generateToken } = require('../helpers/jwt.helper');

function login(req, res){
  let credentials;
  let response;

  req.on('data', (chunk) => {
    credentials = chunk;
  });

  req.on('end', async () => {
    credential = JSON.parse(credentials);
    const { email, password } = credential;
    if(!email || !password){
      response = {
        error: sendError(400, 'parametros incorrectos')
      }
      res.end(JSON.stringify(response));
    }
    const userExist = await User.findOne({email});
    if(!userExist){
      response = {
        error: sendError(404, 'El usuario no existe')
      }
      res.end(JSON.stringify(response));
    }
    
    const validPassword = userExist.comparePassword(password);
    if (!validPassword) {
      response = {
        error: sendError(400, "Contraseña incorrecta")
      }
      res.end(JSON.stringify(response));
    }
    
    const userToEncode = {
      email: userExist.email,
      id: userExist._id,
      isAdmin: userExist.isAdmin
    }

    const token = generateToken(userToEncode);
    
    response = {
      token: token,
      user: userExist
    }
    
    res.end(JSON.stringify(response));
   
  })
}

function logout(req, res){
  res.end('logout');
}

module.exports = {
  login,
  logout
}