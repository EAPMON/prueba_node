const Server = require('../server/index');
const { ENV } = require('./env');

const authServer = new Server(ENV.host, ENV.port, 'auth');

module.exports = authServer;
require('./auth.routes');
