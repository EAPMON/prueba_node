const { sendError } = require('../helpers/error.helper');
const Word = require('../models/word.model');
const jwt = require("jsonwebtoken");
const { JWT_SECRET } = require('../config/index');

function create(req, res){
  let word = null;
  let response;
  
  req.on('data', (chunk) => {
    word = chunk;
  });
   
  req.on('end', async () => {
   let wordNew = JSON.parse(word);
   
   if(typeof wordNew.word !== 'string'){
     response = {
      error: sendError(400, 'el parametro no es una palabra')
     }
    res.end(JSON.stringify(response));
   }

   const reverse = wordNew.word.split('').reverse().join(''); 
   if(wordNew.word != reverse){
    wordNew['isPalindrome'] = false;
   }else{
    wordNew['isPalindrome'] = true;
   }

   response = await Word.create(wordNew);
   res.end(JSON.stringify(response));
  });

  
  
}

function destroy (req, res){
  let id;
  let userEncode;
  let response;
  req.on('data', (chunk) => {
    id = chunk;
  });

  req.on('end', async () => {
    const word = JSON.parse(id);
    const token = req.headers["authorization"];
    if(!token){
      response = {
        error: sendError(401, 'token no enviado')
      }
      res.end(JSON.stringify(response));
    }
    jwt.verify(token, JWT_SECRET, (err, decodedToken) => {
      if(err){
        response = {
          error: sendError(401, 'token inalido')
        }
        res.end(JSON.stringify(response));
      }
      userEncode = decodedToken;
    });

    //res.end(JSON.stringify(user));

    if(!userEncode.user.isAdmin){
      response = {
        error: sendError(401, 'No es administrador')
      }
      res.end(JSON.stringify(response))
    }
    
    response = await Word.findByIdAndDelete(word.id) ;


    res.end(JSON.stringify(response));
  })
}

module.exports = {
  create,
  destroy
}