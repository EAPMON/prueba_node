const { create, destroy } = require('./palindrome.controller');
const palindromeServer = require('./index');

palindromeServer.routing('/create', create);
palindromeServer.routing('/destroy', destroy);