const Server = require('../server/index');
const { ENV } = require('./env');

const palindromeServer = new Server(ENV.host, ENV.port, 'paindrome');

module.exports = palindromeServer;
require('./palindrome.routes');