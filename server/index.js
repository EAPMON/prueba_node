const http = require('http');
const urls = require('url');
const RegistryController =require('../registry/registry.controller');



class Server {
  
  constructor(host, port, name){
    this.host = host;
    this.port = port;
    this.name = name;
    this.serve = http;
    this.routes = {};
  }

 routing(path, controller){
  this.routes[path] = controller;
 }

  start(){
    this.serve.createServer((request, response) => {
      const reqUrl = urls.parse(request.url).pathname;
      if( typeof this.routes[reqUrl] == 'function') {
        console.log({
          Server: this.name,
          Action: this.routes[reqUrl]
        });
        this.routes[reqUrl](request, response);
      }
    }).listen(this.port, this.host, () => { 
      console.log(`${this.name} runing on http://${this.host}:${this.port}/`);
    })
  }
}

module.exports = Server;