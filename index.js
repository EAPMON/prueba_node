const authServer = require('./auth/index');
const palindromeServer = require('./palindrome/index');
const recordServer = require('./record/index');
const registryServer = require('./registry/index'); 
const mongoose = require('mongoose');
const { MONGO_DATABASE, MONGO_HOST } = require('./config/index');

mongoose.connect(`mongodb://${MONGO_HOST}/${MONGO_DATABASE}`, {
  useNewUrlParser: true, 
  useUnifiedTopology: true 
}).then(() => {
  authServer.start();
  palindromeServer.start();
  recordServer.start();
  registryServer.start(); 
}).catch(console.log)
