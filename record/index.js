const Server = require('../server/index');
const { ENV } = require('./env');

const recordServer = new Server(ENV.host, ENV.port, 'record');

module.exports = recordServer;
require('./record.routes');