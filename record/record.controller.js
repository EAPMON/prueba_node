const Word = require('../models/word.model');

async function getAll(req, res){
  let response;
  response = await Word.find();
  res.end(JSON.stringify(response));
  
}

module.exports = {
  getAll
}