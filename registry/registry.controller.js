const validateUser = require('../helpers/validateUser.helper');
const { sendError } = require('../helpers/error.helper');
const User = require('../models/user.model');

function signUp(req, res){
  let user = null;
  let response;
  
  req.on('data', (chunk) => {
    user = chunk;
  });
   
  req.on('end', async () => {
   let newUser = JSON.parse(user);
   const { error } = validateUser(newUser);
   if(error){
     response = {
      error: error.details[0].message
     }
    res.end(JSON.stringify(response));
   }
   let { email } = newUser;
   const userExist = await User.findOne({ email });
   if(userExist){
     response = sendError(400, "Este usuario ya existe")
     res.end(JSON.stringify(response));
   }
   response = await User.create(newUser);
   res.end(JSON.stringify(response));
  });

  
  
}

module.exports = {
  signUp
}