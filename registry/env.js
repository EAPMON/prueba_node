const { MODE } = require('../config/index');

const envs = {
  dev: {
    host: '127.0.0.1',
    port: 8018
  },
  prod: {
    host: '0.0.0.0',
    port: 8019
  }
}

const env = envs[MODE];

module.exports = {
  ENV: env
}
