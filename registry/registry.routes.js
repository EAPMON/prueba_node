const registryServer = require('./index');
const { signUp } = require('./registry.controller');

registryServer.routing('/register', signUp);
