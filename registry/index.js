const Server = require('../server/index');
const { ENV } = require('./env');

const registryServer = new Server(ENV.host, ENV.port, 'registry');

module.exports = registryServer;
require('./registry.routes');