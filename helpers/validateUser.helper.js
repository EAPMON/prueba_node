const Joi = require('@hapi/joi');

function validateUser(user){
  const schema = Joi.object({
    name: Joi.string().required(),
    email: Joi.string().required(),
    password: Joi.string().required(),
    isAdmin: Joi.boolean().required()
  })

  return schema.validate(user);
}

module.exports = validateUser;